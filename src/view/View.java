package view;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class View extends JFrame{
	
	private JButton button ;
	private JPanel panel1 ;
	private JPanel panel2 ;
	public JTextField textField ;
	public JComboBox combo ;
	public JTextArea textArea ;
	private JLabel label ;
	
	public View () {
		createLabel() ;
		createButton() ;
		createTextField() ;
		createComboBox() ;
		createTextArea() ;
		createPanel() ;		
	}
	
	public void createLabel () {
		label = new JLabel("Select type : ") ;
		label.setBounds(80, 150, 160, 30);
	}
	
	public void createButton () {
		button = new JButton("Run") ;
		button.setBounds(110,300,80,40);
	}
	
	public void createTextField () {
		textField = new JTextField() ;
		textField.setBounds(80,100,160,30);
	}
	
	public void createComboBox () {
		String type[] = {"1" , "2" , "3" , "4"} ;
		combo = new JComboBox(type) ;
		combo.setBounds(80,200,160,30);
	}
	
	public void createTextArea () {
		textArea = new JTextArea() ;
		textArea.setBounds(321, 20, 240, 420);
		textArea.setBackground(Color.LIGHT_GRAY);
	}
	
	public void createPanel () {
		panel1 = new JPanel() ;
		panel1.setSize(300,500) ;
		panel1.add(button) ;
		panel1.add(textField) ;
		panel1.add(combo) ;	
		panel1.add(label) ;
		panel1.setLayout(null);
		add(panel1) ;
		
		panel2 = new JPanel() ;
		panel2.setSize(301, 500);
		panel2.add(textArea) ;
		panel2.setLayout(null) ;
		panel2.setBackground(Color.white);
		add(panel2) ;
	}
	
	public void setListener(ActionListener list) {
		button.addActionListener(list);
	}

}
