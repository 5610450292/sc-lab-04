package control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

import model.Model1;
import model.Model2;
import model.Model3;
import model.Model4;
import view.View;

public class Control {
	
	ActionListener list ;
	View view ;
	Model1 model1 ;
	Model2 model2 ;
	Model3 model3 ;
	Model4 model4 ;
	

	class AddInterestListener implements ActionListener 
	{
		public void actionPerformed(ActionEvent event)
	    {
			int num = Integer.parseInt(view.textField.getText()) ;
			//view.textField.setText("") ;
			String menu = (String)view.combo.getSelectedItem() ;
			if (menu == "1") {
				model1 = new Model1(num) ;
				view.textArea.setText(model1.Type()) ;
			}
			else if (menu == "2") {
				model2 = new Model2(num) ;
				view.textArea.setText(model2.Type()) ;
			}
			else if (menu == "3") {
				model3 = new Model3(num) ;
				view.textArea.setText(model3.Type()) ;
			}
			else {
				model4 = new Model4(num) ;
				view.textArea.setText(model4.Type()) ;
			}
	    }
	}
	
	public static void main (String[] args) {
		new Control() ;
	}
	
	public Control () {
		view = new View() ;
		view.pack();
		view.setVisible(true) ;
		view.setSize(600, 500) ;
		view.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE) ;
		list = new AddInterestListener() ;
		view.setListener(list) ;
		
		
		
	}
}
